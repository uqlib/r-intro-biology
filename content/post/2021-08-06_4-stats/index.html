---
title: "4. R and statistics: Introduction to ANOVA, linear models, and tidymodels"
author: "UQ Library"
date: "2021-08-05"
output:
  blogdown::html_page:
    toc: true
weight: 4
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>

<div id="TOC">
<ul>
<li><a href="#what-are-we-going-to-learn">What are we going to learn?</a></li>
<li><a href="#essential-shortcuts">Essential shortcuts</a></li>
<li><a href="#material">Material</a></li>
<li><a href="#about-the-data">About the data</a></li>
<li><a href="#statistics-in-r-using-base-and-stats">Statistics in R using base and stats</a>
<ul>
<li><a href="#visualize-the-data">Visualize the data</a></li>
<li><a href="#analysis-of-variance-anova">Analysis of Variance (ANOVA)</a></li>
<li><a href="#linear-model">Linear Model</a></li>
</ul></li>
<li><a href="#introducing-tidymodels">Introducing Tidymodels</a>
<ul>
<li><a href="#build-and-fit-a-model">Build and fit a model</a></li>
</ul></li>
<li><a href="#use-a-model-to-predict">Use a model to predict</a></li>
<li><a href="#close-project">Close project</a></li>
<li><a href="#useful-links">Useful links</a></li>
</ul>
</div>

<div id="what-are-we-going-to-learn" class="section level2">
<h2>What are we going to learn?</h2>
<p>During this session, you will:</p>
<ul>
<li>analysis of variance (ANOVA) in base R</li>
<li>linear models in base R</li>
<li>the tidymodel approach</li>
</ul>
</div>
<div id="essential-shortcuts" class="section level2">
<h2>Essential shortcuts</h2>
<p>Remember some of the most commonly used RStudio shortcuts:</p>
<ul>
<li>function or dataset help: press <kbd>F1</kbd> with your cursor anywhere in a function name.</li>
<li>execute from script: <kbd>Ctrl</kbd> + <kbd>Enter</kbd></li>
<li>assignment operator (<code>&lt;-</code>): <kbd>Alt</kbd> + <kbd>-</kbd></li>
</ul>
</div>
<div id="material" class="section level2">
<h2>Material</h2>
<p><strong>Load the following packages</strong>:</p>
<pre class="r"><code>library(tidymodels) # for parsnip package and rest of tidymodels

# helper packages
library(readr)       # for importing data
library(car)         # Companion to Applied Regression package
library(performance) # Assessment of Regressmon Models performance
library(dotwhisker)# for visualizing regression results
library(parsnip)</code></pre>
<blockquote>
<p>Remember to use <kbd>Ctrl</kbd>+<kbd>Enter</kbd> to execute a command from the script.</p>
</blockquote>
</div>
<div id="about-the-data" class="section level2">
<h2>About the data</h2>
<p>The following section will be using data from <a href="https://link.springer.com/article/10.1007/BF00349318">Constable (1993)</a> to explore how three different feeding regimes affect the size of sea urchins over time.</p>
<p>Sea urchins reportedly regulate their size according to the level of food available to regulate maintenance requirements. The paper examines whether a reduction in suture width (i.e., connection points between plates; see Fig 1 from constable 1993) is the basis for shrinking due to low food conditions.</p>
<div class="figure">
<img src="images/Constable-1993-fig1.PNG" alt="" />
<p class="caption">Figure 1 from Constable 1993 paper showing sea urchin plates and suture width</p>
</div>
<p>The <a href="https://tidymodels.org/start/models/urchins.csv">data in csv format</a> is available from the tidymodels website and were assembled for a tutorial <a href="https://www.flutterbys.com.au/stats/tut/tut7.5a.html">here</a>.</p>
<pre class="r"><code>urchins &lt;- 
   # read in the data
   read_csv(&quot;https://tidymodels.org/start/models/urchins.csv&quot;) %&gt;% 
   # change the names to be more description
setNames(c(&quot;food_regime&quot;, &quot;initial_volume&quot;, &quot;width&quot;)) %&gt;% 
   # convert food_regime from chr to a factor, helpful for modeling
   mutate(food_regime = factor(food_regime, 
                               levels = c(&quot;Initial&quot;, &quot;Low&quot;, &quot;High&quot;)))</code></pre>
<pre class="r"><code>urchins # see the data as a tibble</code></pre>
<pre><code>## # A tibble: 72 x 3
##    food_regime initial_volume width
##    &lt;fct&gt;                &lt;dbl&gt; &lt;dbl&gt;
##  1 Initial                3.5 0.01 
##  2 Initial                5   0.02 
##  3 Initial                8   0.061
##  4 Initial               10   0.051
##  5 Initial               13   0.041
##  6 Initial               13   0.061
##  7 Initial               15   0.041
##  8 Initial               15   0.071
##  9 Initial               16   0.092
## 10 Initial               17   0.051
## # ... with 62 more rows</code></pre>
<p>We have 72 urchins with data on:</p>
<ul>
<li>experimental feeding regime group with 3 levels (Initial, Low, or High)</li>
<li>size in milliliters at the start of the experiment (initial_volume)</li>
<li>suture width in millimeters at the end of the experiment (width, see <a href="pictures/Constable-1993-fig1.PNG">Fig 1</a>)</li>
</ul>
</div>
<div id="statistics-in-r-using-base-and-stats" class="section level2">
<h2>Statistics in R using base and stats</h2>
<div id="visualize-the-data" class="section level3">
<h3>Visualize the data</h3>
<p>Use a boxplot to visualize width versus <code>food_regime</code> as a factor and a scatterplot for width versus <code>initial_volume</code> as a continuous variable.</p>
<pre class="r"><code>boxplot(width ~ food_regime, data = urchins)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-2-1.png" width="672" /></p>
<pre class="r"><code>plot(width ~ initial_volume, data = urchins)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-2-2.png" width="672" /></p>
<p>We can see that there are some relationships between the response variable (width) and our two covariates (food_regime and initial volume). But what about the interaction between the two covariates?</p>
<p><strong>Challenge 1 - use ggplot2 to make a plot visualizing the interaction between our two variables. Add a trendline to the data.</strong></p>
<blockquote>
<p>Hint: think about grouping and coloring.</p>
</blockquote>
<pre class="r"><code>ggplot(urchins,
       aes(x = initial_volume,
           y = width,
           group = food_regime,
           col = food_regime)) +
   geom_point() +
   geom_smooth(method = lm, se = FALSE) +
   scale_colour_viridis_d(option = &quot;plasma&quot;, end = 0.7)</code></pre>
<pre><code>## `geom_smooth()` using formula &#39;y ~ x&#39;</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-3-1.png" width="672" /></p>
<p>Urchins that were larger in volume at the start of the experiment tended to have wider sutures at the end. Slopes of the lines look different so this effect may depend on the feeding regime indicating we should include an interaction term.</p>
</div>
<div id="analysis-of-variance-anova" class="section level3">
<h3>Analysis of Variance (ANOVA)</h3>
<p>Information in this section was taken from <a href="https://rpubs.com/tmcurley/twowayanova">rpubs.com</a> and <a href="https://bookdown.org/steve_midway/DAR/understanding-anova-in-r.html#multiple-comparisons">Data Analysis in R Ch 7</a>.</p>
<p>We can do an ANOVA with the <code>aov()</code> function to test for differences in sea urchin suture width between our groups. We are technically running and <strong>analysis of covariance (ANCOVA)</strong> as we have both a continuous and a categorical variable. ANOVAs are for categorical variables and we will see that some of the <em>post-hoc</em> tests are not amenable to continuous variables.</p>
<blockquote>
<p><code>aov()</code> uses the model formula <code>response variable ~ covariate1 + covariate2</code>. The * denotes the inclusion of both main effects and interactions which we have done below. The formula below is equivalent to <code>reponse ~ covar1 + covar2 + covar1:covar2</code> i.e. the main effect of covar 1 and covar 2, and the interaction between the two.</p>
</blockquote>
<pre class="r"><code>aov_urch &lt;- aov(width ~ food_regime * initial_volume, 
                data = urchins)
summary(aov_urch)  # print the summary statistics</code></pre>
<pre><code>##                            Df   Sum Sq  Mean Sq F value   Pr(&gt;F)    
## food_regime                 2 0.012380 0.006190  13.832 9.62e-06 ***
## initial_volume              1 0.008396 0.008396  18.762 5.15e-05 ***
## food_regime:initial_volume  2 0.004609 0.002304   5.149  0.00835 ** 
## Residuals                  66 0.029536 0.000448                     
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1</code></pre>
<p>Both the main effects and interaction are significant (p &lt; 0.05) indicating a significant interactive effect between food regime and initial volume on urchin suture width. We need to do a pairwise-comparison to find out which factor levels and combination of the two covariates have the largest effect on width.</p>
<div id="pair-wise-comparison" class="section level4">
<h4>Pair-wise comparison</h4>
<p>Run a <strong>Tukey’s Honestly Significant Difference (HSD)</strong> test - note it does not work for non-factors as per the warning message.</p>
<pre class="r"><code>TukeyHSD(aov_urch)</code></pre>
<pre><code>## Warning in replications(paste(&quot;~&quot;, xx), data = mf): non-factors ignored:
## initial_volume</code></pre>
<pre><code>## Warning in replications(paste(&quot;~&quot;, xx), data = mf): non-factors ignored:
## food_regime, initial_volume</code></pre>
<pre><code>## Warning in TukeyHSD.aov(aov_urch): &#39;which&#39; specified some non-factors which will
## be dropped</code></pre>
<pre><code>##   Tukey multiple comparisons of means
##     95% family-wise confidence level
## 
## Fit: aov(formula = width ~ food_regime * initial_volume, data = urchins)
## 
## $food_regime
##                      diff          lwr         upr     p adj
## Low-Initial  -0.006791667 -0.021433881 0.007850548 0.5100502
## High-Initial  0.023791667  0.009149452 0.038433881 0.0006687
## High-Low      0.030583333  0.015941119 0.045225548 0.0000129</code></pre>
<p>The comparison between High-Initial and High-Low food regimes are significant (p &lt; 0.05).</p>
</div>
<div id="checking-the-model" class="section level4">
<h4>Checking the model</h4>
<p>We also want to check that our model is a good fit and does not violate any ANOVA <strong>assumptions</strong>:</p>
<ol style="list-style-type: decimal">
<li>Data are independent and normally distributed.</li>
<li>The residuals from the data are normally distributed.</li>
<li>The variances of the sampled populations are equal.</li>
</ol>
<p><strong>Challenge 2 - Use a histogram and qqplots to visually check data are normal</strong>.</p>
<pre class="r"><code>hist(urchins$width)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-6-1.png" width="672" /></p>
<pre class="r"><code>qqnorm(urchins$width)
qqline(urchins$width)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-6-2.png" width="672" /></p>
<p>You could also run a Shapiro-Wilk test on the data:</p>
<pre class="r"><code>shapiro.test(urchins$width)</code></pre>
<pre><code>## 
##  Shapiro-Wilk normality test
## 
## data:  urchins$width
## W = 0.95726, p-value = 0.01552</code></pre>
<p>Check the <strong>model residuals</strong>. Plot the residuals vs fitted values - do not want too much deviation from 0.</p>
<pre class="r"><code>plot(aov_urch, 1)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-8-1.png" width="672" /></p>
<pre class="r"><code>plot(predict(aov_urch) ~ aov_urch$fitted.values)
abline(0, 1, col = &quot;red&quot;)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-8-2.png" width="672" /></p>
<p>Check the <strong>normality of residuals</strong>, run Shapiro-Wilk test on residuals:</p>
<pre class="r"><code>plot(aov_urch, 2)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-9-1.png" width="672" /></p>
<pre class="r"><code>shapiro.test(resid(aov_urch))</code></pre>
<pre><code>## 
##  Shapiro-Wilk normality test
## 
## data:  resid(aov_urch)
## W = 0.98456, p-value = 0.5244</code></pre>
<p>The residuals fall on the Normal Q-Q plot diagonal and the Shapiro-Wilk result is non-significant (p &gt; 0.05).</p>
<p>Check for <strong>homogeneity of variance</strong></p>
<p><strong>Challenge 3 - use the help documentation for <code>leveneTest()</code> from the <code>car</code> package to check homogenetity of variance on <code>food_regime</code>. </strong></p>
<blockquote>
<p>Again, only works for factor groups.</p>
</blockquote>
<pre class="r"><code>leveneTest(width ~ food_regime, data = urchins)</code></pre>
<pre><code>## Levene&#39;s Test for Homogeneity of Variance (center = median)
##       Df F value  Pr(&gt;F)  
## group  2  4.4224 0.01559 *
##       69                  
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1</code></pre>
<p>The Levene’s Test is significant for <code>food_regime</code> (not what we want) and there are a few options to deal with this. You can ignore this violation based on your own <em>a priori</em> knowledge of the distribution of the population being samples, drop the p-value significance, or use a different test.</p>
</div>
</div>
<div id="linear-model" class="section level3">
<h3>Linear Model</h3>
<pre class="r"><code>lm_urch &lt;- lm(width ~ food_regime * initial_volume, 
              data = urchins)
summary(lm_urch)</code></pre>
<pre><code>## 
## Call:
## lm(formula = width ~ food_regime * initial_volume, data = urchins)
## 
## Residuals:
##       Min        1Q    Median        3Q       Max 
## -0.045133 -0.013639  0.001111  0.013226  0.067907 
## 
## Coefficients:
##                                  Estimate Std. Error t value Pr(&gt;|t|)    
## (Intercept)                     0.0331216  0.0096186   3.443 0.001002 ** 
## food_regimeLow                  0.0197824  0.0129883   1.523 0.132514    
## food_regimeHigh                 0.0214111  0.0145318   1.473 0.145397    
## initial_volume                  0.0015546  0.0003978   3.908 0.000222 ***
## food_regimeLow:initial_volume  -0.0012594  0.0005102  -2.469 0.016164 *  
## food_regimeHigh:initial_volume  0.0005254  0.0007020   0.748 0.456836    
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
## 
## Residual standard error: 0.02115 on 66 degrees of freedom
## Multiple R-squared:  0.4622, Adjusted R-squared:  0.4215 
## F-statistic: 11.35 on 5 and 66 DF,  p-value: 6.424e-08</code></pre>
<p>In the output, we have the model call, residuals, and the coefficients. The first coefficient is the <code>(Intercept)</code> and you might notice the <code>food_regimeInitial</code> is missing. The function defaults to an effects parameterization where the intercept is the reference or baseline of the categorical group - Initial in this case.
&gt;You can change the reference level of a factor using the <code>relevel()</code> function.</p>
<p>The esimates of the remaining group levels of <code>food_regime</code> represents the effect of being in that group. To calculate the group coefficients for all group levels you <em>add</em> the estimates for the level to the intercept (first group level) estimate. For example, the estimate for the ‘Initial’ feeding regime is 0.0331 and we add the estimate of ‘Low’ (0.0331 + 0.0197) to get the mean maximum size of 0.0528 mm for width.</p>
<p>For the continuous covariate, the estimate represents the change in the response variable for a unit increase in the covariate. ‘Initial Volume’s’ estimate of 0.0015 represents a 0.0015 mm increase (the estimate is positive) in width per ml increase in urchin initial volume.</p>
<p>We can get ANOVA test statistics on our linear model using the <code>anova()</code> in base or <code>Anova()</code> from the <code>car</code> package.</p>
<pre class="r"><code>anova(lm_urch)</code></pre>
<pre><code>## Analysis of Variance Table
## 
## Response: width
##                            Df    Sum Sq   Mean Sq F value    Pr(&gt;F)    
## food_regime                 2 0.0123801 0.0061900 13.8321 9.616e-06 ***
## initial_volume              1 0.0083962 0.0083962 18.7621 5.154e-05 ***
## food_regime:initial_volume  2 0.0046088 0.0023044  5.1494  0.008354 ** 
## Residuals                  66 0.0295358 0.0004475                      
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1</code></pre>
<pre class="r"><code>Anova(lm_urch)</code></pre>
<pre><code>## Anova Table (Type II tests)
## 
## Response: width
##                               Sum Sq Df F value    Pr(&gt;F)    
## food_regime                0.0168653  2 18.8434 3.358e-07 ***
## initial_volume             0.0083962  1 18.7621 5.154e-05 ***
## food_regime:initial_volume 0.0046088  2  5.1494  0.008354 ** 
## Residuals                  0.0295358 66                      
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1</code></pre>
<p>These are effectively the same as the <code>aov()</code> model we ran before.</p>
<blockquote>
<p><strong>Note</strong>: The statistics outputs are the same comparing the <code>aov()</code> and <code>anova()</code> models while the <code>Anova()</code> model is <strong>not</strong> exactly the same. The <code>Anova()</code> output tells us it was a Type II test and the <code>aov()</code> documentation says it is only for <em>balanced</em> designs which means the Type 1 test is the applied (see <a href="https://bookdown.org/ndphillips/YaRrr/type-i-type-ii-and-type-iii-anovas.html">here</a>). The type of test can be set for <code>Anova()</code> but not the others. Here, the overall take-away from the different ANOVA functions are comparable.</p>
</blockquote>
<p><strong>Challenge 4 - use the check_model() documentation to apply the function to our <code>lm_urch</code> model.</strong></p>
<p>The performance package has a handy function <code>check_model()</code> that will check several aspects of your model in one go:</p>
<pre class="r"><code>check_model(lm_urch)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-14-1.png" width="672" /></p>
</div>
</div>
<div id="introducing-tidymodels" class="section level2">
<h2>Introducing Tidymodels</h2>
<p>Like the tidyverse package, the tidymodels framework is a collection of packages for modeling and machine learning following the tidyverse principles.</p>
<p>This section is modified from the first <a href="https://www.tidymodels.org/start/models/">Tidymodels article</a>.</p>
<div id="build-and-fit-a-model" class="section level3">
<h3>Build and fit a model</h3>
<p>Let’s apply a standard two-way analysis of variance (ANOVA) model to the dataset as we did before. For this kind of model, ordinary least squares is a good initial approach.</p>
<p>For Tidymodels, we need to specify the following:</p>
<ol style="list-style-type: decimal">
<li>The <em>functional form</em> using the <a href="https://parsnip.tidymodels.org/">parsnip package</a>.</li>
<li>The <em>method for fitting</em> the model by setting the <strong>engine</strong>.</li>
</ol>
<p>We will specify the <em>functional form</em> or model type as <a href="https://parsnip.tidymodels.org/reference/linear_reg.html">“linear regression”</a> as there is a numeric outcome with a linear slope and intercept. We can do this with:</p>
<pre class="r"><code>linear_reg()  </code></pre>
<pre><code>## Linear Regression Model Specification (regression)
## 
## Computational engine: lm</code></pre>
<p>On its own, not that interesting. Next, we specify the method for <em>fitting</em> or training the model using the <code>set_engine()</code> function. The engine value is often a mash-up of the software that can be used to fit or train the model as well as the estimation method. For example, to use ordinary least squares, we can set the engine to be <code>lm</code>.</p>
<p>The <a href="https://parsnip.tidymodels.org/reference/linear_reg.html">documentation page</a> for linear_reg() lists the possible engines. We’ll save this model object as lm_mod.</p>
<pre class="r"><code>lm_mod &lt;- 
linear_reg() %&gt;% 
   set_engine(&quot;lm&quot;)</code></pre>
<p>Next, the model can be estimated or trained using the <code>fit()</code> function and the model formula we used for the ANOVAs:</p>
<p><code>width ~ initial_volume * food_regime</code></p>
<pre class="r"><code>lm_fit &lt;- 
   lm_mod %&gt;% 
   fit(width ~ initial_volume * food_regime, data = urchins)

lm_fit</code></pre>
<pre><code>## parsnip model object
## 
## Fit time:  0ms 
## 
## Call:
## stats::lm(formula = width ~ initial_volume * food_regime, data = data)
## 
## Coefficients:
##                    (Intercept)                  initial_volume  
##                      0.0331216                       0.0015546  
##                 food_regimeLow                 food_regimeHigh  
##                      0.0197824                       0.0214111  
##  initial_volume:food_regimeLow  initial_volume:food_regimeHigh  
##                     -0.0012594                       0.0005254</code></pre>
<p>We can use the <code>tidy()</code> function for our <code>lm</code> object to output model parameter estimates and their statistical properties. Similar to <code>summary()</code> but the results are more predictable and useful format.</p>
<pre class="r"><code>tidy(lm_fit)</code></pre>
<pre><code>## # A tibble: 6 x 5
##   term                            estimate std.error statistic  p.value
##   &lt;chr&gt;                              &lt;dbl&gt;     &lt;dbl&gt;     &lt;dbl&gt;    &lt;dbl&gt;
## 1 (Intercept)                     0.0331    0.00962      3.44  0.00100 
## 2 initial_volume                  0.00155   0.000398     3.91  0.000222
## 3 food_regimeLow                  0.0198    0.0130       1.52  0.133   
## 4 food_regimeHigh                 0.0214    0.0145       1.47  0.145   
## 5 initial_volume:food_regimeLow  -0.00126   0.000510    -2.47  0.0162  
## 6 initial_volume:food_regimeHigh  0.000525  0.000702     0.748 0.457</code></pre>
<p>This output can be used to generate a dot-and-whisker plot of our regression results using the <code>dotwhisker</code> package:</p>
<pre class="r"><code>tidy(lm_fit) %&gt;% 
   dwplot(dot_args = list(size = 2, color = &quot;black&quot;),
          whisker_args = list(color = &quot;black&quot;),
          vline = geom_vline(xintercept = 0, 
                             color = &quot;grey50&quot;,
                             linetype = 2))</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-19-1.png" width="672" /></p>
</div>
</div>
<div id="use-a-model-to-predict" class="section level2">
<h2>Use a model to predict</h2>
<p>Say that it would be interesting to make a plot of the mean body size for urchins that started the experiement with an initial volume of 20 ml.</p>
<p>First, lets make some new example data to predict for our graph:</p>
<pre class="r"><code>new_points &lt;- expand.grid(initial_volume = 20,
                          food_regime = c(&quot;Initial&quot;, &quot;Low&quot;, &quot;High&quot;))
new_points</code></pre>
<pre><code>##   initial_volume food_regime
## 1             20     Initial
## 2             20         Low
## 3             20        High</code></pre>
<p>We can then use the <code>predict()</code> function to find the mean values at 20 ml initial volume.</p>
<p>With tidymodels, the types of predicted values are standardized so that we can use the same syntax to get these values.</p>
<p>Let’s generate the mean suture width values:</p>
<pre class="r"><code>mean_pred &lt;- predict(lm_fit, new_data = new_points)
mean_pred</code></pre>
<pre><code>## # A tibble: 3 x 1
##    .pred
##    &lt;dbl&gt;
## 1 0.0642
## 2 0.0588
## 3 0.0961</code></pre>
<p>When making predictions, the tidymodels convention is to always produce a tibble of results with standardized column names. This makes it easy to combine the original data and the predictions in a usable format:</p>
<pre class="r"><code>conf_int_pred &lt;- predict(lm_fit, 
                         new_data = new_points,
                         type = &quot;conf_int&quot;)
conf_int_pred</code></pre>
<pre><code>## # A tibble: 3 x 2
##   .pred_lower .pred_upper
##         &lt;dbl&gt;       &lt;dbl&gt;
## 1      0.0555      0.0729
## 2      0.0499      0.0678
## 3      0.0870      0.105</code></pre>
<pre class="r"><code># now combine:
plot_data &lt;- 
   new_points %&gt;% 
   bind_cols(mean_pred, conf_int_pred)

plot_data</code></pre>
<pre><code>##   initial_volume food_regime      .pred .pred_lower .pred_upper
## 1             20     Initial 0.06421443  0.05549934  0.07292952
## 2             20         Low 0.05880940  0.04986251  0.06775629
## 3             20        High 0.09613343  0.08696233  0.10530453</code></pre>
<pre class="r"><code># and plot:
ggplot(plot_data, 
       aes(x = food_regime)) +
   geom_point(aes(y = .pred)) +
   geom_errorbar(aes(ymin = .pred_lower,
                     ymax = .pred_upper),
                 width = .2) +
   labs(y = &quot;urchin size&quot;)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-22-1.png" width="672" /></p>
<p>There is also an example of a <em>Bayesian</em> model in the tidymodels article I have not included here.</p>
</div>
<div id="close-project" class="section level2">
<h2>Close project</h2>
<p>Closing RStudio will ask you if you want to save your workspace and scripts.
Saving your workspace is usually not recommended if you have all the necessary commands in your script.</p>
</div>
<div id="useful-links" class="section level2">
<h2>Useful links</h2>
<ul>
<li>For statistical analysis in R:
<ul>
<li>Steve Midway’s <a href="https://bookdown.org/steve_midway/DAR/part-ii-analysis.html">Data Analysis in R Part II Analysis</a></li>
<li>Jeffrey A. Walker’s <a href="https://www.middleprofessor.com/files/applied-biostatistics_bookdown/_book/">Applied Statistics for Experiemental Biology</a></li>
<li>Chester Ismay and Albert Y. Kim’s <a href="https://moderndive.com/">ModernDive Statistical Inference via Data Science</a></li>
</ul></li>
<li>For tidymodels:
<ul>
<li><a href="https://www.tidymodels.org/">tidymodels website</a></li>
</ul></li>
<li>Our compilation of <a href="https://gitlab.com/stragu/DSH/blob/master/R/usefullinks.md">general R resources</a></li>
</ul>
</div>
