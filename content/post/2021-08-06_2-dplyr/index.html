---
title: "2. R data manipulation with RStudio and dplyr"
author: "UQ Library"
date: "2021-08-05"
output:
  blogdown::html_page:
    toc: true
weight: 2
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>

<div id="TOC">
<ul>
<li><a href="#what-are-we-going-to-learn">What are we going to learn?</a></li>
<li><a href="#gapminder-data">Gapminder data</a></li>
<li><a href="#basic-dplyr-verbs">Basic dplyr verbs</a>
<ul>
<li><a href="#pick-variables-with-select">1. Pick variables with <code>select()</code></a></li>
<li><a href="#pick-observations-with-filter">2. Pick observations with <code>filter()</code></a></li>
<li><a href="#reorder-observations-with-arrange">3. Reorder observations with <code>arrange()</code></a></li>
<li><a href="#create-new-variables-with-mutate">4. Create new variables with <code>mutate()</code></a></li>
<li><a href="#collapse-to-a-single-value-with-summarise">5. Collapse to a single value with <code>summarise()</code></a></li>
<li><a href="#change-the-scope-with-group_by">6. Change the scope with <code>group_by()</code></a></li>
</ul></li>
<li><a href="#more-examples">More examples</a></li>
<li><a href="#what-next">What next?</a></li>
</ul>
</div>

<div id="what-are-we-going-to-learn" class="section level2">
<h2>What are we going to learn?</h2>
<p>In this hands-on session, you will use R, RStudio and the <code>dplyr</code> package to transform your data.</p>
<p>Specifically, you will learn how to <strong>explore, filter, reorganise and process</strong> a table of data with the following verbs:</p>
<ul>
<li><code>select()</code>: pick variables</li>
<li><code>filter()</code>: pick observations</li>
<li><code>arrange()</code>: reorder observations</li>
<li><code>mutate()</code>: create new variables</li>
<li><code>summarise()</code>: collapse to a single summary</li>
<li><code>group_by()</code>: change the scope of function</li>
</ul>
</div>
<div id="gapminder-data" class="section level2">
<h2>Gapminder data</h2>
<p>You should already have the data ready from the introduction chapter, but if you need to import it again, run the following command (to read directly from the online CSV):</p>
<pre class="r"><code>gapminder &lt;- read.csv(&quot;https://raw.githubusercontent.com/resbaz/r-novice-gapminder-files/master/data/gapminder-FiveYearData.csv&quot;)</code></pre>
</div>
<div id="basic-dplyr-verbs" class="section level2">
<h2>Basic dplyr verbs</h2>
<p>The R package <code>dplyr</code> was developed by Hadley Wickham for data manipulation.</p>
<p>The book <em><a href="https://r4ds.had.co.nz/">R for Data Science</a></em> introduces the package as follows:</p>
<blockquote>
<p>You are going to learn the five key dplyr functions that allow you to solve the vast majority of your data manipulation challenges:</p>
<ul>
<li>Pick variables by their names with <code>select()</code></li>
<li>Pick observations by their values with <code>filter()</code></li>
<li>Reorder the rows with <code>arrange()</code></li>
<li>Create new variables with functions of existing variables with <code>mutate()</code></li>
<li>Collapse many values down to a single summary with <code>summarise()</code></li>
</ul>
<p>These can all be used in conjunction with <code>group_by()</code> which changes the scope of each function from operating on the entire dataset to operating on it group-by-group. These six functions provide the main <strong>verbs for a language of data manipulation</strong>.</p>
</blockquote>
<p>To use the verbs to their full extent, we will need <strong>pipes</strong> and <strong>logical operators</strong>, which we will introduce as we go.</p>
<p>Let’s load the <code>dplyr</code> package to access its functions:</p>
<pre class="r"><code>library(dplyr)</code></pre>
<pre><code>## 
## Attaching package: &#39;dplyr&#39;</code></pre>
<pre><code>## The following objects are masked from &#39;package:stats&#39;:
## 
##     filter, lag</code></pre>
<pre><code>## The following objects are masked from &#39;package:base&#39;:
## 
##     intersect, setdiff, setequal, union</code></pre>
<blockquote>
<p>You only need to install a package once (with <code>install.packages()</code>), but you need to reload it every time you start a new R session (with <code>library()</code>).</p>
</blockquote>
<div id="pick-variables-with-select" class="section level3">
<h3>1. Pick variables with <code>select()</code></h3>
<p><code>select()</code> allows us to pick variables (i.e. columns) from the dataset. For example, to only keep the data about year, country and GDP per capita:</p>
<pre class="r"><code>gap_small &lt;- select(gapminder, year, country, gdpPercap)</code></pre>
<p>The first argument refers to the dataframe that is being transformed, and the following arguments are the columns you want to keep. Notice that it keeps the order you specified?</p>
<p>You can also rename columns in the same command:</p>
<pre class="r"><code>gap_small &lt;- select(gapminder, year, country, gdpPerPerson = gdpPercap)</code></pre>
<p>Finally, if you have many variables but only want to remove a small number, it might be better to deselect instead of selecting. You can do that by using the <code>-</code> character in front of a variable name:</p>
<pre class="r"><code>names(select(gapminder, -continent))</code></pre>
<pre><code>## [1] &quot;country&quot;   &quot;year&quot;      &quot;pop&quot;       &quot;lifeExp&quot;   &quot;gdpPercap&quot;</code></pre>
</div>
<div id="pick-observations-with-filter" class="section level3">
<h3>2. Pick observations with <code>filter()</code></h3>
<p>The <code>filter()</code> function allows use to pick observations depending on one or several conditions. But to be able to define these conditions, we need to learn about logical operators.</p>
<p><strong>Logical operators</strong> allow us to <strong>compare things</strong>. Here are some of the most important ones:</p>
<ul>
<li><code>==</code>: equal</li>
<li><code>!=</code>: different</li>
<li><code>&gt;</code>: greater than</li>
<li><code>&lt;</code>: smaller than</li>
<li><code>&gt;=</code>: greater or equal</li>
<li><code>&lt;=</code>: smaller or equal</li>
</ul>
<blockquote>
<p>Remember: <code>=</code> is used to pass on a value to an argument, whereas <code>==</code> is used to check for equality.</p>
</blockquote>
<p>You can compare any kind of data For example:</p>
<pre class="r"><code>1 == 1</code></pre>
<pre><code>## [1] TRUE</code></pre>
<pre class="r"><code>1 == 2</code></pre>
<pre><code>## [1] FALSE</code></pre>
<pre class="r"><code>1 &gt; 0</code></pre>
<pre><code>## [1] TRUE</code></pre>
<pre class="r"><code>&quot;money&quot; == &quot;happiness&quot;</code></pre>
<pre><code>## [1] FALSE</code></pre>
<p>When R executes these commands, it answers <code>TRUE</code> of <code>FALSE</code>, as if asked a yes/no question. These <code>TRUE</code> and <code>FALSE</code> values are called <strong>logical values</strong>.</p>
<p>Note that we can compare a single value to many. For example, compare one value to five other:</p>
<pre class="r"><code>1 == c(1, 2, 3, 1, 3)</code></pre>
<pre><code>## [1]  TRUE FALSE FALSE  TRUE FALSE</code></pre>
<p>This kind of operation results in a logical vector with a logical value for each element. This is exactly what we will use to filter our rows.</p>
<p>For example, to filter the observations for Australia, we can use the following condition:</p>
<pre class="r"><code>australia &lt;- filter(gapminder, country == &quot;Australia&quot;)
australia</code></pre>
<pre><code>##      country year      pop continent lifeExp gdpPercap
## 1  Australia 1952  8691212   Oceania  69.120  10039.60
## 2  Australia 1957  9712569   Oceania  70.330  10949.65
## 3  Australia 1962 10794968   Oceania  70.930  12217.23
## 4  Australia 1967 11872264   Oceania  71.100  14526.12
## 5  Australia 1972 13177000   Oceania  71.930  16788.63
## 6  Australia 1977 14074100   Oceania  73.490  18334.20
## 7  Australia 1982 15184200   Oceania  74.740  19477.01
## 8  Australia 1987 16257249   Oceania  76.320  21888.89
## 9  Australia 1992 17481977   Oceania  77.560  23424.77
## 10 Australia 1997 18565243   Oceania  78.830  26997.94
## 11 Australia 2002 19546792   Oceania  80.370  30687.75
## 12 Australia 2007 20434176   Oceania  81.235  34435.37</code></pre>
<p>The function compares the value “Australia” to all the values in the <code>country</code> variable, and only keeps the rows that have <code>TRUE</code> as an answer.</p>
<p>Now, let’s filter the rows that have a life expectancy <code>lifeExp</code> greater than 81 years:</p>
<pre class="r"><code>life81 &lt;- filter(gapminder, lifeExp &gt; 81)
dim(life81)</code></pre>
<pre><code>## [1] 7 6</code></pre>
</div>
<div id="reorder-observations-with-arrange" class="section level3">
<h3>3. Reorder observations with <code>arrange()</code></h3>
<p><code>arrange()</code> will reorder our rows according to a variable, by default in ascending order:</p>
<pre class="r"><code>arrange(life81, lifeExp)</code></pre>
<pre><code>##           country year       pop continent lifeExp gdpPercap
## 1       Australia 2007  20434176   Oceania  81.235  34435.37
## 2 Hong Kong China 2002   6762476      Asia  81.495  30209.02
## 3     Switzerland 2007   7554661    Europe  81.701  37506.42
## 4         Iceland 2007    301931    Europe  81.757  36180.79
## 5           Japan 2002 127065841      Asia  82.000  28604.59
## 6 Hong Kong China 2007   6980412      Asia  82.208  39724.98
## 7           Japan 2007 127467972      Asia  82.603  31656.07</code></pre>
<p>If we want to have a look at the entries with highest life expectancy first, we can use the <code>desc()</code> function (for “descending”):</p>
<pre class="r"><code>arrange(life81, desc(lifeExp))</code></pre>
<pre><code>##           country year       pop continent lifeExp gdpPercap
## 1           Japan 2007 127467972      Asia  82.603  31656.07
## 2 Hong Kong China 2007   6980412      Asia  82.208  39724.98
## 3           Japan 2002 127065841      Asia  82.000  28604.59
## 4         Iceland 2007    301931    Europe  81.757  36180.79
## 5     Switzerland 2007   7554661    Europe  81.701  37506.42
## 6 Hong Kong China 2002   6762476      Asia  81.495  30209.02
## 7       Australia 2007  20434176   Oceania  81.235  34435.37</code></pre>
<p>We could also use the <code>-</code> shortcut, which only works for numerical data:</p>
<pre class="r"><code>arrange(life81, -lifeExp)</code></pre>
<div id="the-pipe-operator" class="section level4">
<h4>The pipe operator</h4>
<p>What if we wanted to get that result in one single command, without an intermediate <code>life81</code> object?</p>
<p>We could nest the commands into each other, the first step as the first argument of the second step:</p>
<pre class="r"><code>arrange(filter(gapminder, lifeExp &gt; 81), -lifeExp)</code></pre>
<p>… but this becomes very hard to read, very quickly. (Imagine with 3 steps or more!)</p>
<p>We can make our code more readable and avoid creating useless intermediate objects by <strong>piping</strong> commands into each other. The pipe operator <code>%&gt;%</code> <strong>strings commands together</strong>, using the left side’s output as the first argument of the right side function.</p>
<p>For example, this command:</p>
<pre class="r"><code>round(1.23, digits = 1)</code></pre>
<pre><code>## [1] 1.2</code></pre>
<p>… is equivalent to:</p>
<pre class="r"><code>1.23 %&gt;% round(digits = 1)</code></pre>
<pre><code>## [1] 1.2</code></pre>
<p>Here’s another example with the <code>filter()</code> verb:</p>
<pre class="r"><code>gapminder %&gt;%
  filter(country != &quot;France&quot;)</code></pre>
<p>… becomes:</p>
<pre class="r"><code>filter(gapminder, country != &quot;France&quot;)</code></pre>
<p>To do what we did previously in one single command, using the pipe:</p>
<pre class="r"><code>gapminder %&gt;% 
  filter(lifeExp &gt; 81) %&gt;% 
  arrange(-lifeExp)</code></pre>
<pre><code>##           country year       pop continent lifeExp gdpPercap
## 1           Japan 2007 127467972      Asia  82.603  31656.07
## 2 Hong Kong China 2007   6980412      Asia  82.208  39724.98
## 3           Japan 2002 127065841      Asia  82.000  28604.59
## 4         Iceland 2007    301931    Europe  81.757  36180.79
## 5     Switzerland 2007   7554661    Europe  81.701  37506.42
## 6 Hong Kong China 2002   6762476      Asia  81.495  30209.02
## 7       Australia 2007  20434176   Oceania  81.235  34435.37</code></pre>
<p>The pipe operator can be read as “then” and makes the code a lot <strong>more readable</strong> than when nesting functions into each other, and avoids the creation of several intermediate objects. It is also easier to trouble shoot as it makes it easy to execute the pipeline step by step.</p>
<p>From now on, we’ll use the pipe syntax as a default.</p>
<blockquote>
<p>Note that this material uses the <code>magrittr</code> pipe. The <code>magrittr</code> package is the one that introduced the pipe operator to the R world, and <code>dplyr</code> automatically imports this useful operator when it is loaded. However, the pipe being such a widespread and popular concept in programming and data science, it ended up making it into Base R (the “native” pipe) in 2021 with the release of R 4.1, using a different operator: <code>|&gt;</code>. You can switch your pipe shortcut to the native pipe in <code>Tools &gt; Global options &gt; Code &gt; Use native pipe operator</code>.</p>
</blockquote>
</div>
<div id="challenge-1-a-tiny-dataset" class="section level4">
<h4>Challenge 1 – a tiny dataset</h4>
<p>Select the 2002 life expectancy observation for Eritrea (and remove the rest of the variables).</p>
<pre class="r"><code>eritrea_2002 &lt;- gapminder %&gt;%
    select(year, country, lifeExp) %&gt;%
    filter(country == &quot;Eritrea&quot;, year == 2002)</code></pre>
</div>
</div>
<div id="create-new-variables-with-mutate" class="section level3">
<h3>4. Create new variables with <code>mutate()</code></h3>
<p>Have a look at what the verb <code>mutate()</code> can do with <code>?mutate</code>.</p>
<p>Let’s see what the two following variables can be used for:</p>
<pre class="r"><code>gapminder %&gt;%
  select(gdpPercap, pop) %&gt;%
  head()</code></pre>
<pre><code>##   gdpPercap      pop
## 1  779.4453  8425333
## 2  820.8530  9240934
## 3  853.1007 10267083
## 4  836.1971 11537966
## 5  739.9811 13079460
## 6  786.1134 14880372</code></pre>
<p>How do you think we could combine them to add something new to our dataset?</p>
<div id="challenge-2-mutate-the-gdp" class="section level4">
<h4>Challenge 2 – mutate the GDP</h4>
<p>Use <code>mutate()</code> to create a <code>gdp</code> variable.</p>
<p>Name your new dataset <code>gap_gdp</code>. When finished, <code>dim(gap_gdp)</code> should result in <code>1704 7</code>.</p>
<p>Hint: use the <code>*</code> operator within <code>mutate()</code>.</p>
<pre class="r"><code>gap_gdp &lt;- gapminder %&gt;%
    mutate(gdp = gdpPercap * pop)
dim(gap_gdp)</code></pre>
<pre><code>## [1] 1704    7</code></pre>
<pre class="r"><code>head(gap_gdp)</code></pre>
<pre><code>##       country year      pop continent lifeExp gdpPercap         gdp
## 1 Afghanistan 1952  8425333      Asia  28.801  779.4453  6567086330
## 2 Afghanistan 1957  9240934      Asia  30.332  820.8530  7585448670
## 3 Afghanistan 1962 10267083      Asia  31.997  853.1007  8758855797
## 4 Afghanistan 1967 11537966      Asia  34.020  836.1971  9648014150
## 5 Afghanistan 1972 13079460      Asia  36.088  739.9811  9678553274
## 6 Afghanistan 1977 14880372      Asia  38.438  786.1134 11697659231</code></pre>
<p>You can reuse a variable computed by ‘mutate()’ straight away. For example, we also want a more readable version of our new variable, in billion dollars:</p>
<pre class="r"><code>gap_gdp &lt;- gapminder %&gt;%
    mutate(gdp = gdpPercap * pop,
           gdpBil = gdp / 1e9)</code></pre>
</div>
</div>
<div id="collapse-to-a-single-value-with-summarise" class="section level3">
<h3>5. Collapse to a single value with <code>summarise()</code></h3>
<p><code>summarise()</code> collapses many values down to a single summary. For example, to find the mean life expectancy for the whole dataset:</p>
<pre class="r"><code>gapminder %&gt;%
  summarise(meanLE = mean(lifeExp))</code></pre>
<pre><code>##     meanLE
## 1 59.47444</code></pre>
<p>However, a single-value summary is not particularly interesting. <code>summarise()</code> becomes more powerful when used with <code>group_by()</code>.</p>
</div>
<div id="change-the-scope-with-group_by" class="section level3">
<h3>6. Change the scope with <code>group_by()</code></h3>
<p><code>group_by()</code> changes the scope of the following function(s) from operating on the entire dataset to operating on it group-by-group.</p>
<p>See the effect of the grouping step:</p>
<pre class="r"><code>gapminder %&gt;%
    group_by(continent)</code></pre>
<pre><code>## # A tibble: 1,704 × 6
## # Groups:   continent [5]
##    country      year      pop continent lifeExp gdpPercap
##    &lt;chr&gt;       &lt;int&gt;    &lt;dbl&gt; &lt;chr&gt;       &lt;dbl&gt;     &lt;dbl&gt;
##  1 Afghanistan  1952  8425333 Asia         28.8      779.
##  2 Afghanistan  1957  9240934 Asia         30.3      821.
##  3 Afghanistan  1962 10267083 Asia         32.0      853.
##  4 Afghanistan  1967 11537966 Asia         34.0      836.
##  5 Afghanistan  1972 13079460 Asia         36.1      740.
##  6 Afghanistan  1977 14880372 Asia         38.4      786.
##  7 Afghanistan  1982 12881816 Asia         39.9      978.
##  8 Afghanistan  1987 13867957 Asia         40.8      852.
##  9 Afghanistan  1992 16317921 Asia         41.7      649.
## 10 Afghanistan  1997 22227415 Asia         41.8      635.
## # … with 1,694 more rows</code></pre>
<p>The data in the cells is the same, the size of the object is the same. However, the dataframe was converted to a <strong>tibble</strong>, because a dataframe is not capable of storing grouping information.</p>
<p>Using the <code>group_by()</code> function before summarising makes things more interesting. For example, to find out the total population per continent in 2007, we can do the following:</p>
<pre class="r"><code>gapminder %&gt;% 
    filter(year == 2007) %&gt;%
    group_by(continent) %&gt;%
    summarise(pop = sum(pop))</code></pre>
<pre><code>## # A tibble: 5 × 2
##   continent        pop
##   &lt;chr&gt;          &lt;dbl&gt;
## 1 Africa     929539692
## 2 Americas   898871184
## 3 Asia      3811953827
## 4 Europe     586098529
## 5 Oceania     24549947</code></pre>
<div id="challenge-3-max-life-expectancy-per-country" class="section level4">
<h4>Challenge 3 – max life expectancy per country</h4>
<p>Group by country, and find out the maximum life expectancy ever recorded for each one.</p>
<p>Hint: <code>?max</code></p>
<pre class="r"><code>gapminder %&gt;% 
    group_by(country) %&gt;%
    summarise(maxLE = max(lifeExp))</code></pre>
<pre><code>## # A tibble: 142 × 2
##    country     maxLE
##    &lt;chr&gt;       &lt;dbl&gt;
##  1 Afghanistan  43.8
##  2 Albania      76.4
##  3 Algeria      72.3
##  4 Angola       42.7
##  5 Argentina    75.3
##  6 Australia    81.2
##  7 Austria      79.8
##  8 Bahrain      75.6
##  9 Bangladesh   64.1
## 10 Belgium      79.4
## # … with 132 more rows</code></pre>
</div>
</div>
</div>
<div id="more-examples" class="section level2">
<h2>More examples</h2>
<p>Another example of a summary, with a different dataset that dplyr provides:</p>
<pre class="r"><code>starwars %&gt;%
  group_by(species) %&gt;%
  summarise(
    n = n(), # this counts the number of rows in each group
    mass = mean(mass, na.rm = TRUE)
  ) %&gt;%
  filter(n &gt; 1) # the mean of a single value is not worth reporting</code></pre>
<pre><code>## # A tibble: 9 × 3
##   species      n  mass
##   &lt;chr&gt;    &lt;int&gt; &lt;dbl&gt;
## 1 Droid        6  69.8
## 2 Gungan       3  74  
## 3 Human       35  82.8
## 4 Kaminoan     2  88  
## 5 Mirialan     2  53.1
## 6 Twi&#39;lek      2  55  
## 7 Wookiee      2 124  
## 8 Zabrak       2  80  
## 9 &lt;NA&gt;         4  48</code></pre>
</div>
<div id="what-next" class="section level2">
<h2>What next?</h2>
<p>More on dplyr:</p>
<ul>
<li><a href="https://github.com/rstudio/cheatsheets/raw/master/data-transformation.pdf">dplyr cheatsheet</a></li>
<li><em>R for Data Science</em>, <a href="http://r4ds.had.co.nz/transform.html">chapter about dplyr</a></li>
</ul>
<p>For further R resources, look at <a href="https://gitlab.com/stragu/DSH/blob/master/R/usefullinks.md">our compilation of resources</a>.</p>
</div>
