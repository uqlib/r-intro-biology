---
title: "3. R data visualisation with RStudio and ggplot2: introduction"
author: "UQ Library"
date: "2021-08-05"
output:
  blogdown::html_page:
    toc: true
weight: 3
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>

<div id="TOC">
<ul>
<li><a href="#what-are-we-going-to-learn">What are we going to learn?</a></li>
<li><a href="#essential-shortcuts">Essential shortcuts</a></li>
<li><a href="#material">Material</a>
<ul>
<li><a href="#introducing-ggplot2">Introducing ggplot2</a></li>
<li><a href="#the-components-of-the-grammar-of-graphics">The components of the Grammar of Graphics</a></li>
<li><a href="#ggplot2s-three-essential-components">ggplot2’s three essential components</a></li>
<li><a href="#scatterplots">Scatterplots</a></li>
<li><a href="#layering">Layering</a></li>
<li><a href="#adding-aesthetics">Adding aesthetics</a></li>
<li><a href="#saving-a-plot">Saving a plot</a></li>
<li><a href="#faceting">Faceting</a></li>
<li><a href="#customising-a-plot">Customising a plot</a></li>
</ul></li>
<li><a href="#play-time">Play time!</a></li>
<li><a href="#close-project">Close project</a></li>
<li><a href="#useful-links">Useful links</a></li>
</ul>
</div>

<div id="what-are-we-going-to-learn" class="section level2">
<h2>What are we going to learn?</h2>
<p>During this session, you will:</p>
<ul>
<li>Have a visualisation package installed (ggplot2)</li>
<li>Learn how to explore data visually</li>
<li>Learn about the 3 essential ggplot2 components</li>
<li>Use different kinds of visualisations</li>
<li>Layer several visualisations</li>
<li>Learn how to customise a plot with colours, labels and themes.</li>
</ul>
</div>
<div id="essential-shortcuts" class="section level2">
<h2>Essential shortcuts</h2>
<p>Remember some of the most commonly used RStudio shortcuts:</p>
<ul>
<li>function or dataset help: press <kbd>F1</kbd> with your cursor anywhere in a function name.</li>
<li>execute from script: <kbd>Ctrl</kbd> + <kbd>Enter</kbd></li>
<li>assignment operator (<code>&lt;-</code>): <kbd>Alt</kbd> + <kbd>-</kbd></li>
</ul>
</div>
<div id="material" class="section level2">
<h2>Material</h2>
<p>If you haven’t loaded the whole Tidyverse yet, we can <strong>load the ggplot2 package</strong> on its own by running the following command:</p>
<pre class="r"><code>library(ggplot2)</code></pre>
<blockquote>
<p>Remember to use <kbd>Ctrl</kbd>+<kbd>Enter</kbd> to execute a command from the script.</p>
</blockquote>
<div id="introducing-ggplot2" class="section level3">
<h3>Introducing ggplot2</h3>
<p>The R package ggplot2 was developed by Hadley Wickham with the objective of creating a grammar of graphics for categorical data (in 2007). It is based on the book <em>The Grammar of Graphics</em> Developed by Leland Wilkinson (first edition published in 1999).</p>
<p>It is now part of the group of data science packages called Tidyverse.</p>
</div>
<div id="the-components-of-the-grammar-of-graphics" class="section level3">
<h3>The components of the Grammar of Graphics</h3>
<p>The Grammar of Graphics is based on the idea that you can build every graph from the same few components.</p>
<p>The components are:</p>
<ul>
<li>Data</li>
<li>Mapping</li>
<li>Statistics</li>
<li>Scales</li>
<li>Geometries</li>
<li>Facets</li>
<li>Coordinates</li>
<li>Theme</li>
</ul>
<p>In this introductory session, we will mainly focus on the <strong>data</strong>, the <strong>mapping</strong>, the <strong>statistics</strong>, the <strong>geometries</strong> and the <strong>theme</strong>.</p>
</div>
<div id="ggplot2s-three-essential-components" class="section level3">
<h3>ggplot2’s three essential components</h3>
<p>In ggplot2, the 3 main components that we usually have to provide are:</p>
<ol style="list-style-type: decimal">
<li>Where the <strong>data</strong> comes from,</li>
<li>the <strong>aesthetic mappings</strong>, and</li>
<li>a <strong>geometry</strong>.</li>
</ol>
<p>For our first example, let’s use the <code>msleep</code> dataset (from the ggplot2 package), which contains data about mammals’ sleeping patterns.</p>
<blockquote>
<p>You can find out about the dataset with <code>?msleep</code>.</p>
</blockquote>
<p>Let’s start with specifying where the <strong>data</strong> comes from in the <code>ggplot()</code> function:</p>
<pre class="r"><code>ggplot(data = msleep)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-2-1.png" width="672" /></p>
<p>This is not very interesting. We need to tell ggplot2 <em>what</em> we want to visualise, by <strong>mapping</strong> <em>aesthetic elements</em> (like our axes) to <em>variables</em> from the data. We want to visualise how common different conservations statuses are, so let’s associate the right variable to the x axis:</p>
<pre class="r"><code>ggplot(data = msleep,
       mapping = aes(x = conservation))</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-3-1.png" width="672" /></p>
<p>ggplot2 has done what we asked it to do: the conservation variable is on the x axis. But nothing is shown on the plot area, because we haven’t defined <em>how</em> to represent the data, with a <code>geometry_*</code> function:</p>
<pre class="r"><code>ggplot(data = msleep,
       mapping = aes(x = conservation)) +
  geom_bar()</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-4-1.png" width="672" /></p>
<p>Now we have a useful plot: we can see that a lot of animals in this dataset don’t have a conservation status, and that “least concern” is the next most common value.</p>
<p>We can see our three essential elements in the code:</p>
<ol style="list-style-type: decimal">
<li>the <strong>data</strong> comes from the <code>msleep</code> object;</li>
<li>the variable <code>conservation</code> is <strong>mapped to the aesthetic</strong> <code>x</code> (i.e. the x axis);</li>
<li>the <strong>geometry</strong> is <code>"bar"</code>, for “bar chart”.</li>
</ol>
<p>Here, we don’t need to specify what variable is associated to the y axis, as the “bar” geometry automatically does a count of the different values in the <code>conservation</code> variable. That is what <strong>statistics</strong> are applied automatically to the data.</p>
<blockquote>
<p>In ggplot2, each geometry has default statistics, so we often don’t need to specify which stats we want to use. We could use a <code>stat_*()</code> function instead of a <code>geom_*()</code> function, but most people start with the geometry (and let ggplot2 pick the default statistics that are applied).</p>
</blockquote>
</div>
<div id="scatterplots" class="section level3">
<h3>Scatterplots</h3>
<p>Let’s have a look at another dataset: the <code>penguins</code> dataset from the <code>palmerpenguins package</code>.</p>
<pre class="r"><code>library(palmerpenguins)
penguins</code></pre>
<pre><code>## # A tibble: 344 × 8
##    species island    bill_length_mm bill_depth_mm flipper_length_mm body_mass_g
##    &lt;fct&gt;   &lt;fct&gt;              &lt;dbl&gt;         &lt;dbl&gt;             &lt;int&gt;       &lt;int&gt;
##  1 Adelie  Torgersen           39.1          18.7               181        3750
##  2 Adelie  Torgersen           39.5          17.4               186        3800
##  3 Adelie  Torgersen           40.3          18                 195        3250
##  4 Adelie  Torgersen           NA            NA                  NA          NA
##  5 Adelie  Torgersen           36.7          19.3               193        3450
##  6 Adelie  Torgersen           39.3          20.6               190        3650
##  7 Adelie  Torgersen           38.9          17.8               181        3625
##  8 Adelie  Torgersen           39.2          19.6               195        4675
##  9 Adelie  Torgersen           34.1          18.1               193        3475
## 10 Adelie  Torgersen           42            20.2               190        4250
## # … with 334 more rows, and 2 more variables: sex &lt;fct&gt;, year &lt;int&gt;</code></pre>
<p>Learn more about it with <code>?penguins</code>, and have a peak at its structure with:</p>
<pre class="r"><code>str(economics)</code></pre>
<pre><code>## spec_tbl_df [574 × 6] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
##  $ date    : Date[1:574], format: &quot;1967-07-01&quot; &quot;1967-08-01&quot; ...
##  $ pce     : num [1:574] 507 510 516 512 517 ...
##  $ pop     : num [1:574] 198712 198911 199113 199311 199498 ...
##  $ psavert : num [1:574] 12.6 12.6 11.9 12.9 12.8 11.8 11.7 12.3 11.7 12.3 ...
##  $ uempmed : num [1:574] 4.5 4.7 4.6 4.9 4.7 4.8 5.1 4.5 4.1 4.6 ...
##  $ unemploy: num [1:574] 2944 2945 2958 3143 3066 ...</code></pre>
<p>Scatterplots are often used to look at the relationship between two variables. Let’s look at the relationship between bill length and bill depth:</p>
<pre class="r"><code>ggplot(data = penguins,
       mapping = aes(x = bill_length_mm,
                     y = bill_depth_mm)) +
    geom_point()</code></pre>
<pre><code>## Warning: Removed 2 rows containing missing values (geom_point).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-7-1.png" width="672" /></p>
<p>Let’s go through our essential elements once more:</p>
<ul>
<li>The <code>ggplot()</code> function initialises a ggplot object. In it, we declare the <strong>input data frame</strong> and specify the set of plot aesthetics used throughout all layers of our plot;</li>
<li>The <code>aes()</code> function groups our <strong>mappings of aesthetics to variables</strong>;</li>
<li>The <code>geom_&lt;...&gt;()</code> function specifies what <strong>geometric element</strong> we want to use.</li>
</ul>
<p>It’s hard to see any kind of trend in there, but we might be missing something, so let’s add a trend line on top.</p>
</div>
<div id="layering" class="section level3">
<h3>Layering</h3>
<p>A trend line can be created with the <code>geom_smooth()</code> function. How can we combine several layers? We can string them with the <code>+</code> operator:</p>
<pre class="r"><code>ggplot(data = penguins,
       mapping = aes(x = bill_length_mm,
                     y = bill_depth_mm)) +
  geom_point() +
  geom_smooth()</code></pre>
<pre><code>## `geom_smooth()` using method = &#39;loess&#39; and formula &#39;y ~ x&#39;</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_smooth).</code></pre>
<pre><code>## Warning: Removed 2 rows containing missing values (geom_point).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-8-1.png" width="672" /></p>
<p>The console shows you what function / formula was used to draw the trend line. This is important information, as there are countless ways to do that. To better understand what happens in the background, open the function’s help page and notice that the default value for the <code>method</code> argument is “NULL”. Read up on how it automatically picks a suitable method depending on the sample size, in the “Arguments” section.</p>
<p>Want a linear trend line instead? Add the argument <code>method = "lm"</code> to your function:</p>
<pre class="r"><code>ggplot(data = penguins,
       mapping = aes(x = bill_length_mm,
                     y = bill_depth_mm)) +
  geom_point() +
  geom_smooth(method = &quot;lm&quot;)</code></pre>
<pre><code>## `geom_smooth()` using formula &#39;y ~ x&#39;</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_smooth).</code></pre>
<pre><code>## Warning: Removed 2 rows containing missing values (geom_point).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-9-1.png" width="672" /></p>
<p>A linear model makes it look like the relationship is negative… We might have to reveal more information to have a better understanding of it.</p>
</div>
<div id="adding-aesthetics" class="section level3">
<h3>Adding aesthetics</h3>
<p>We can highlight the “species” factor by adding a new aesthetic:</p>
<pre class="r"><code>ggplot(data = penguins,
       mapping = aes(x = bill_length_mm,
                     y = bill_depth_mm,
                     colour = species)) +
  geom_point() +
  geom_smooth(method = &quot;lm&quot;)</code></pre>
<pre><code>## `geom_smooth()` using formula &#39;y ~ x&#39;</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_smooth).</code></pre>
<pre><code>## Warning: Removed 2 rows containing missing values (geom_point).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-10-1.png" width="672" /></p>
<p>It now makes a lot more sense: by splitting the data into different species, we can see that the two variables a positively correlated. The longer the beak, the deeper it usually is. We just witnessed <a href="https://en.wikipedia.org/wiki/Simpson%27s_paradox">Simpson’s paradox</a>, in which omitting important variables in the analysis leads to inaccurate interpretations.</p>
<blockquote>
<p>The order of the functions matters: the points will be drawn before the trend line, which is probably what you’re after.</p>
</blockquote>
<p><strong>Challenge 1 – where should aesthetics be defined?</strong></p>
<p>Take the last plot we created:</p>
<pre class="r"><code>ggplot(data = penguins,
       mapping = aes(x = bill_length_mm,
                     y = bill_depth_mm,
                     colour = species)) +
  geom_point() +
  geom_smooth(method = &quot;lm&quot;)</code></pre>
<p>How would you go about going back to drawing one single trend line for the whole population?</p>
<p>Hint: see <code>?geom_point()</code></p>
<p>Different geometries can also have their own mappings that overwrite the defaults.
If you place mappings in a geom function, ggplot2 will treat them as local mappings for the layer. It will use these mappings to extend or overwrite the global mappings for that layer only. This makes it possible to display different aesthetics in different layers.</p>
</div>
<div id="saving-a-plot" class="section level3">
<h3>Saving a plot</h3>
<p>Like your visualisation? You can export it with the “Export” menu in the “Plots” pane.</p>
<ul>
<li>Building a document or a slideshow? You can copy it straight to your clipboard, and paste it into it.</li>
<li>A PDF is a good, quick option to export an easily shareable file with vector graphics. Try for example the “A5” size, the “Landscape” orientation, and save it into your “plots” directory.</li>
<li>More options are available in the “Save as image…” option. PNG is a good compressed format for graphics, but if you want to further customise your visualisation in a different program, use SVG or EPS, which are vector formats. (Try to open an SVG file in <a href="https://inkscape.org/">Inkscape</a> for example.)</li>
</ul>
<p>To save the last plot with a command, you can use the <code>ggsave()</code> function:</p>
<pre class="r"><code>ggsave(filename = &quot;plots/bills.png&quot;)</code></pre>
<p>This is great to automate the export process for each plot in your script, but <code>ggsave()</code> also has extra options, like setting the DPI, which is useful for getting the right resolution for a specific use. For example, to export a plot for printing on a poster, you can use a higher definition with the <code>dpi</code> argument:</p>
<pre class="r"><code>ggsave(filename = &quot;plots/bills_poster.png&quot;, dpi = 600)</code></pre>
<p><strong>Challenge 2 – compare the distribution of flipper lengths</strong></p>
<p>A few geometry functions can help us explore how different species have different flipper lengths:</p>
<ul>
<li><code>geom_histogram()</code></li>
<li><code>geom_density()</code></li>
<li><code>geom_boxplot()</code></li>
</ul>
<p>Pick one geometry, try to build a visualisation by matching aesthetics with variables, and share your code with others!</p>
</div>
<div id="faceting" class="section level3">
<h3>Faceting</h3>
<p>Faceting is a powerful feature that often allows to fit another variable into your visualisation.</p>
<p>For example, imagine that you start looking a body mass in different sexes with a boxplot:</p>
<pre class="r"><code>ggplot(penguins, aes(x = sex, y = body_mass_g, colour = sex)) +
  geom_boxplot()</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_boxplot).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-14-1.png" width="672" /></p>
<blockquote>
<p>We stopped using the argument names because we know in which order they appear: first the data, then the mapping of aesthetics. Let’s save ourselves some typing from now on!</p>
</blockquote>
<p>Once more, we might want to add an extra variable to better differentiate different populations:</p>
<pre class="r"><code>ggplot(penguins, aes(x = sex, y = body_mass_g, colour = sex)) +
  geom_boxplot() +
  facet_wrap(vars(species))</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_boxplot).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-15-1.png" width="672" /></p>
<p>As there are three species, the visualisation was split into three facets. We can now see a clearer separation between sexes.</p>
<p>Notice how all the axes are synced? This is a good default, because it allows you to compare data across facets, but you can customise that if you want with the <code>scales</code> argument.</p>
</div>
<div id="customising-a-plot" class="section level3">
<h3>Customising a plot</h3>
<p>Let’s see how we can customise our boxplot’s look.</p>
<div id="change-a-geometrys-default-colour" class="section level4">
<h4>Change a geometry’s default colour</h4>
<p>First, we can pick our favourite colour in <code>geom_boxplot()</code>:</p>
<pre class="r"><code>ggplot(penguins, aes(x = sex, y = body_mass_g, colour = sex)) +
  geom_boxplot(fill = &quot;cornsilk&quot;) +
  facet_wrap(vars(species))</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_boxplot).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-16-1.png" width="672" /></p>
<p>If you are curious about what colour names exist in R, you can use the <code>colours()</code> function.</p>
</div>
<div id="change-labels" class="section level4">
<h4>Change labels</h4>
<p>We can also modify labels with the <code>labs()</code> function, as variable names are not always nice to read.</p>
<p>Let’s have a look at what <code>labs()</code> can do:</p>
<pre class="r"><code>?labs</code></pre>
<p>It can edit the title, the subtitle, the x and y axes labels, the caption, and importantly: the alternative text.</p>
<pre class="r"><code>ggplot(penguins, aes(x = sex, y = body_mass_g, colour = sex)) +
  geom_boxplot(fill = &quot;seashell&quot;) +
  facet_wrap(vars(species)) +
  labs(x = &quot;Sex&quot;,
       y = &quot;Body mass (g)&quot;)</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_boxplot).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-18-1.png" width="672" /></p>
<blockquote>
<p>Remember that captions and titles are better sorted out in the publication itself, especially for accessibility reasons (e.g. to help with screen readers).</p>
</blockquote>
</div>
<div id="themes" class="section level4">
<h4>Themes</h4>
<p>The <code>theme()</code> function allows us to really get into the details of our plot’s look, but some <code>theme_*()</code> functions make it easy to apply a built-in theme, like <code>theme_bw()</code>:</p>
<pre class="r"><code>ggplot(penguins, aes(x = sex, y = body_mass_g, colour = sex)) +
  geom_boxplot(fill = &quot;seashell&quot;) +
  facet_wrap(vars(species)) +
  labs(x = &quot;Sex&quot;,
       y = &quot;Body mass (g)&quot;) +
  theme_bw() + # apply a built-in theme
  theme(legend.position = &quot;none&quot;) # further customise: remove the superfluous legend</code></pre>
<pre><code>## Warning: Removed 2 rows containing non-finite values (stat_boxplot).</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-19-1.png" width="672" /></p>
<p>Try <code>theme_minimal()</code> as well, and if you want more options, install the <code>ggthemes</code> package!</p>
</div>
</div>
</div>
<div id="play-time" class="section level2">
<h2>Play time!</h2>
<p><strong>Challenge 3: explore geometries</strong></p>
<p>When creating a new layer, start typing <code>geom_</code> and see what suggestions pop up. Are there any suggestions that sound useful or familiar to you?</p>
<p>Modify your plots, play around with different layers and functions, and ask questions!</p>
</div>
<div id="close-project" class="section level2">
<h2>Close project</h2>
<p>Closing RStudio will ask you if you want to save your workspace and scripts.
Saving your workspace is usually not recommended if you have all the necessary commands in your script.</p>
</div>
<div id="useful-links" class="section level2">
<h2>Useful links</h2>
<ul>
<li>For ggplot2:
<ul>
<li><a href="https://www.rstudio.org/links/data_visualization_cheat_sheet">ggplot2 cheatsheet</a></li>
<li>Official <a href="http://docs.ggplot2.org/current/">ggplot2 documentation</a></li>
<li>Official <a href="http://ggplot2.tidyverse.org/">ggplot2 website</a></li>
<li><a href="http://r4ds.had.co.nz/data-visualisation.html">Chapter on data visualisation</a> in the book <em>R for Data Science</em></li>
<li>Selva Prabhakaran’s <a href="http://r-statistics.co/ggplot2-Tutorial-With-R.html"><em>r-statistics.co</em> section on ggplot2</a></li>
<li><a href="https://ourcodingclub.github.io/2017/01/29/datavis.html">Coding Club’s data visualisation tutorial</a></li>
<li><a href="http://www.cookbook-r.com/Graphs/">Cookbook for R graphs</a></li>
<li><a href="http://www.sthda.com/english/wiki/ggplot2-essentials">STHDA’s ggplot2 essentials</a></li>
</ul></li>
<li>Our compilation of <a href="https://gitlab.com/stragu/DSH/blob/master/R/usefullinks.md">general R resources</a></li>
</ul>
</div>
