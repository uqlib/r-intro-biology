---
title: "About"
date: '2021-08-05'
---

This website and associated repository were created to publish the supporting material for a one-day workshop on R and RStudio for biology postgraduates at The University of Queensland.

## Workshop Trainers @ UQ Library

![headshot of Stephane Guillou](/./about_files/stragu_pic.png)<br>Stephane Guillou<br>
Technology Trainer<br>

![headshot of Catherine Kim, Asian girl with glasses, blue hair in front of a green leafy wall](/./about_files/ckim_pic.png)<br>
Catherine Kim<br>
Technology Trainer<br>

Contact us via email at training at library dot uq dot edu dot au.

## License

The material is released under a [Creative Commons - Attribution 4.0 International Licence](https://creativecommons.org/licenses/by/4.0/). You may re-use and re-mix the material in any way you wish, without asking permission, provided you cite the original source. However, we'd love to hear about what you do with it!